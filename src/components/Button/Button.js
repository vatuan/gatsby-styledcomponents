import React from 'react'
import { colors } from "../../utils/colors"
import styled from 'styled-components'


const Button = styled.button`
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ width }) => `${width}px`};
  height: 40px;
  /* background: ${props => (props.toggleTheme ? "grey" : props.theme.colors.primary)}; */
  /* background: ${({theme}) => theme.colors.primary}; */
  border-radius: 50px;
  border: none;
  /* color: ${props => (props.toggleTheme ? "black" : colors.white)}; */
  color: ${({theme}) => theme.colors.primary};
  border: 2px solid ${({theme}) => theme.colors.primary};
  font-size: 1em;
  font-weight: ${({theme}) => theme.fonts.bold};
  transition: box-shadow 0.3s ease;
  

  &:hover {
    box-shadow: 0 10px 20px -15px red;
  }

  @media ${({theme}) => theme.media.tablet}{
    background: ${({theme}) => theme.colors.white}
  }
`


export default Button