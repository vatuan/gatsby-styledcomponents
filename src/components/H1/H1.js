import styled from 'styled-components'

const H1 = styled.h1`
  font-size: ${props => (props.isBig ? "3rem" : "1rem")};
  color: ${props => (props.isBlue ? "blue" : "white")};
`
export default H1