import React, { useState } from "react"
import styled, { createGlobalStyle } from "styled-components"
import { Link } from "gatsby"
import { colors } from "../utils/colors"
import Button from "../components/Button/Button"
import H1 from "../components/H1/H1"
import Layout from "../layout/layout"


// Global Style
// const GlobalStyle = createGlobalStyle`
//     body { 
//       padding : 0;
//       margin : 0;
//       font-family : 'Lato';
//       color: white;
//     }
//     * , *::after, *::before { 
//       box-sizing : border-box;
//     }
// `
// //styled components is here
// const StyledWrapper = styled.div`
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   flex-direction: column;
//   background: ${props => (props.toggleTheme ? colors.light : colors.dark)};
//   height: 100vh;
//   position: relative;

  /* ========= Dùng như scss  ===========*/
  /* ::before {
    position: absolute;
    top: 0;
    left: 0;
    content: "";
    width: 50px;
    height: 50px;
    display: block;
    background: blue;
  } */

  /* css cho the h1 nam ben trong div */
  /* h1 {
    color: red;
  } */
  /* css cho div-con */
  /* .div-con {
    width: 100px;
    height: 50px;
    background: green;
  } */
  /* =================== */


// const H1 = styled.h1`
//   font-size: ${props => (props.isBig ? "3rem" : "1rem")};
//   color: ${props => (props.isBlue ? "blue" : "white")};
// `


// const BlueH1 = styled(H1)`
//   color: blue;
// `

// const StyledLink = styled(Link)`
//   text-decoration: none;
//   color: white;
//   font-size: 2em;
//   font-weight: 700;
// `

// const Button = styled.button`
//   text-decoration: none;
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   width: ${({ width }) => width};
//   height: 40px;
//   background: ${props => (props.toggleTheme ? "grey" : colors.primary)};
//   border-radius: 50px;
//   border: none;
//   color: ${props => (props.toggleTheme ? "black" : colors.white)};
//   font-size: 1em;
//   font-weight: 800;
//   transition: box-shadow 0.3s ease;

//   &:hover {
//     box-shadow: 0 10px 20px -15px red;
//   }
// `

// function IndexPage() {
//   const [lightTheme, setLightTheme] = useState(true)
//   const toggleTheme = () => {
//     setLightTheme(!lightTheme)
//   }

//   return (
//     <div>
//       <>
//         <GlobalStyle />
//         <StyledWrapper
//           toggleTheme={lightTheme}
//           className="co-the-dat-className"
//         >
//           {/* <H1 isBig>Hello word</H1>
//       <BlueH1 isBig>Hello word</BlueH1> */}
//           {/* <div className="div-con"></div> */}

//           {/* <StyledLink>About page</StyledLink> */}
//           <H1>Hello word</H1>
//           <Button
//             width="250px"
//             as={Link}
//             to="/about"
//             toggleTheme={lightTheme}
//           >
//             About page
//           </Button>
  
//           <button onClick={toggleTheme}>Toggle Theme</button>
//         </StyledWrapper>
//       </>
//     </div>
//   )
// }


function IndexPage(){
  
  const test = () => {
    alert('van click duoc')
  }
  return (
    <>
  
    <Layout>
      <H1 isBig isBlue={false}>Hello word</H1>
      {/* <Button onClick={test} width='200px'  as={Link} to='/about'>About page</Button> */}
      <Button width='200' as={Link} to='/about'>About page</Button>

    </Layout>
    </>
  )
}
export default IndexPage


