import { Link } from 'gatsby'
// import React from "react"

// function AboutPage() {
//   return (
//     <div>
//       <h1>Hello, this is about page</h1>
//     </div>
//   )
// }

// export default AboutPage
import React from 'react'
import Button from '../components/Button/Button'
import H1 from '../components/H1/H1'
import Layout from '../layout/layout'

function about() {
    return (
       <Layout>
           <H1 isBig>Hello, this is about page</H1>
           <Button width='200' as={Link} to='/'>Go back</Button>
       </Layout>
    )
}

export default about
