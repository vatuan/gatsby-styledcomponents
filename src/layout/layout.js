import React, { useState } from 'react'
import styled, { createGlobalStyle , ThemeProvider} from 'styled-components'
// import { colors } from '../utils/colors'
import { theme } from '../utils/theme'

// Global Style
const GlobalStyle = createGlobalStyle`
    body { 
      padding : 0;
      margin : 0;
      font-family : 'Lato';
      color : white;
    }
    h1{
      color : ${({toogleTheme}) => toogleTheme ? 'black !important' : 'white !important'}
    }
    * , *::after, *::before { 
      box-sizing : border-box;
    }
`
//styled components is here
const StyledWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background: ${props => (props.toogleTheme ? theme.colors.light : theme.colors.dark)};
  height: 100vh;
  position: relative;
 
  `
function Layout({children}) {
  const [lightTheme, setLightTheme] = useState(true)
  const toggleTheme =  () => {
    setLightTheme(!lightTheme)
  }
    return (
        <ThemeProvider theme={theme}>
            <>
            <GlobalStyle/>
          <StyledWrapper>
              {children}
              {/* <button onClick={toggleTheme}>toggle theme</button> */}
          </StyledWrapper>
            </>
        </ThemeProvider>
    )
}

export default Layout
