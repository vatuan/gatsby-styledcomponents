import {colors} from './colors'
export const theme = {
    colors,
    fonts : {
        thin : 300,
        regular : 400,
        bold: 800
    },
    media : {
        phone : 'only screen and (min-width: 768px)',
        tablet : 'only screen and (min-width: 1024px)',
        desktop : 'only screen and (min-width: 1680px)',
    }
}