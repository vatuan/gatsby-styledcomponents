export const colors = {
  white: "#ffffff",
  primary: "#3ec2e9",
  dark: "#0f1c21",
  light: "#F2F3F5;",
}
